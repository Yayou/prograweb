var TableView = function (id, tableModel) {
    this.model = tableModel;

    // Modification du code pour le projet
    // On ajoute ces differents boutons dans un div different, au dessus de celui du tableur

    this.divOpt = document.createElement("div");
    this.divOpt.id = "options-div";

    
    // Bouton gras
    this.boldButton = document.createElement("button");
    this.boldButton.id = "bold";
    this.boldButton.classList.add("style");
    this.boldButton.innerHTML = " a ";
    this.divOpt.appendChild(this.boldButton);

    // Bouton italique
    this.italButton = document.createElement("button");
    this.italButton.id = "italic";
    this.italButton.classList.add("style");
    this.italButton.innerHTML = " a ";
    this.divOpt.appendChild(this.italButton);

    // Bouton souligne
    this.underButton = document.createElement("button");
    this.underButton.id = "underline";
    this.underButton.classList.add("style");
    this.underButton.innerHTML = " a ";
    this.divOpt.appendChild(this.underButton);    

    
    // Bouton couleur du texte
    this.colorButton = document.createElement("button");
    this.colorButton.id = "color";
    this.colorButton.classList.add("style");
    this.colorButton.innerHTML = " Couleur texte ";
    this.divOpt.appendChild(this.colorButton);

    // Bouton couleur de l'arriere plan
    this.bgColButton = document.createElement("button");
    this.bgColButton.id = "bgCol";
    this.bgColButton.classList.add("style");
    this.bgColButton.innerHTML = " Couleur cellule ";
    this.divOpt.appendChild(this.bgColButton);

    // Liste deroulante avec differentes couleurs
    this.listColor = document.createElement("select");
    this.listColor.name = "colChoice";
    this.listColor.id = "listCol";
    this.listColor.classList.add("style");

    var colNames = ["Par defaut", "Noir", "Blanc", "Gris", "Rouge", "Vert", "Jaune", "Bleu", "Violet", "Autre :"];
    var colValues = ["", "black", "white", "gray", "red", "green", "yellow", "blue", "purple", "other"];
    var sizeC = colNames.length;

    // On ajoute toutes les couleurs
    for(var i = 0 ; i < sizeC ; i++) {
	var col = document.createElement("option");
	var val = colValues[i];
	col.value = val;
	if (val != "other" && val != "") {
	    // Pour toutes les lignes correspondant a une couleur, on ajoute l'element aux classes correspondant a cette couleur, aussi bien pour le bg que pour le texte
	    col.classList.add("bg"+val);
	    col.classList.add(val);
	}
	var nam = document.createTextNode(colNames[i]);
	col.appendChild(nam);
	
	this.listColor.appendChild(col);
    }
    
    this.divOpt.appendChild(this.listColor);

    // Div dans lequel on ajoutera l'input permettant de mettre d'autres couleurs si besoin
    var inpDiv = document.createElement("div");
    inpDiv.id = "color_input-div";
    inpDiv.classList.add("style");
    this.divOpt.appendChild(inpDiv);

    // Bouton affichant une fenetre d'aide
    this.helpButton = document.createElement("button");
    this.helpButton.id = "help";
    this.helpButton.classList.add("style");
    this.helpButton.innerHTML = " ? "
    this.divOpt.appendChild(this.helpButton);

    // Lien vers la page contenant la palette des couleurs. Elle s'ouvre en pop-up
    this.linkCol = document.createElement("a");
    this.linkCol.id = "linkCol";
    this.linkCol.href = "#null";
    this.linkCol.setAttribute("onclick","javascript:open_palette();");
    this.linkCol.appendChild(document.createTextNode("Palette des couleurs"));
    this.divOpt.appendChild(this.linkCol);

    // Liste deroulante pour le choix du style des bordures
    this.bordStyle = document.createElement("select");
    this.bordStyle.id = "bordStyle";
    this.bordStyle.name = "bordStyleChoice";
    this.bordStyle.classList.add("style");
    this.divOpt.appendChild(this.bordStyle);

    //On represente les differents styles de bordures en utilisant des elements HTML
    var styleNames = ["Style bordure",
		      "&#8212&#8212&#8212&#8212&#8212",                              // solid
		      "-&#8201-&#8201-&#8201-&#8201-&#8201-&#8201-&#8201-&#8201-",   // dashed
		      "&#8230&#8230&#8230&#8230&#8230"];                             // dotted
    var styleValues = ["", "solid", "dashed", "dotted"];
    var sizeS = styleNames.length;
    
    for (var i = 0 ; i < sizeS ; i++) {
	var styl = document.createElement("option");	
	styl.value = styleValues[i];
	styl.innerHTML = styleNames[i];
	this.bordStyle.appendChild(styl);
    }

    // Liste deroulante pour le choix de la couleur des bordures
    this.bordColor = document.createElement("select");
    this.bordColor.name = "bordColorChoice";
    this.bordColor.id = "bordCol";
    this.bordColor.classList.add("style");
    this.divOpt.appendChild(this.bordColor);

    // On ajoute manuellement une liste decrivant la liste
    var col = document.createElement("option");
    col.value = "";
    col.appendChild(document.createTextNode("Couleur bordure"));
    this.bordColor.appendChild(col);

    // On ne prend pas le dernier element de la liste definie plus haut : il n'y a pas proposition de prendre une autre couleur
    for (var i = 0 ; i < sizeC-1 ; i++) {
	var col = document.createElement("option");
	var val = colValues[i];
	col.value = val;
	if (val != "") {
	    col.classList.add("bg"+val);
	    col.classList.add(val);
	}
	col.appendChild(document.createTextNode(colNames[i]));
	this.bordColor.appendChild(col);
    }

    

    // Fin du code ajouté pour le projet


    
    this.div = document.createElement("div");
    this.div.id = "spreadsheet-div";


    var target = document.getElementById(id);
    if (target) {
	target.appendChild(this.divOpt);
	target.appendChild(this.div);
    }


    this.input = document.createElement("input");
    this.input.type = "text";
    this.div.appendChild(this.input);

    this.button = document.createElement("button");
    this.button.innerHTML = "&#10003;";
    this.div.appendChild(this.button);    

    this.table = document.createElement("table");
    this.table.id = "table";
    this.div.appendChild(this.table);
};

TableView.prototype.createTable = function () {
    var model = this.model;
    var table = this.table;


    //Clear the table

    for (var c = table.firstChild; c != null; c = c.nextSibling)
        table.removeChild(c);



    var thead = document.createElement("thead");
    table.appendChild(thead);

    var tr = document.createElement("tr");
    thead.appendChild(tr);
    tr.appendChild(document.createElement("th"));

    model.forEachCol(function (c) {
        var th = document.createElement("th");
        th.appendChild(document.createTextNode(c));
        tr.appendChild(th);
    });

    var tbody = document.createElement("tbody");
    table.appendChild(tbody);

    model.forEachRow(function (j) {
        var tr = document.createElement("tr");
        tbody.appendChild(tr);
        var td = document.createElement("td");
        var text = document.createTextNode(j);
        td.appendChild(text);
        tr.appendChild(td);
        model.forEachCol(function (i) {
            var cell = model.getCell(i,j);
            var td = document.createElement("td");
            cell.setView(td);

	    //monkey patching
            td.row = j;
            td.col = i;

            td.notify = function (cell) {
                td.firstChild.nodeValue = cell.getValue();
            };

            td.isSelected = function () {
                return this.classList.contains("selected");
            };

            td.select = function (b) {
                if (b)
                    this.classList.add("selected");
                else
                    this.classList.remove("selected");
            };

	    td.id = i+j;

            var text = document.createTextNode(cell.getValue());
            td.appendChild(text);
            tr.appendChild(td);
        });

    });


};

var TableController = function (view) {

    var this_ = this;

    if (!(view instanceof TableView))
        throw "Invalid view";

    function findTD(obj) {
        if ( !obj || obj.nodeName == "TD") return obj
        else findTD(obj.parentNode);
    };


    // On commence par definir des fonctions de base :
    
    // On cree une fonction permettant de gerer les selections de plus d'une case : cette fonction prend en parametre une fonction, et une selection decrite sous la forme d'une liste contenant l'id de la premiere et de la derniere case (par exemple ["A1", "D5"]), et applique la fonction a toutes les cases
    function forSelect(f, select) {

	var colToIdx = function (s) {
            var res = 0;
            for (var i = 0; i < s.length; i++) {
                res *= 26;
                res += (s.charCodeAt(i) - 64);
            }
            return (res - 1);
        };

        var idxToCol = function (i) {
            var res = "";
            var n = i+1;
            var c = 0;
            while (n > 0) {
                c = n % 26;
                c = c == 0 ? 26 : c;
                res = String.fromCharCode(c + 64) + res;
                n = Math.trunc((n-c) / 26) ;
            };
            return res;

        };


	// On recupere les numeros de colonne et de ligne des deux cellules decrivant la selection, eventuellement en convertissant. On utilise pour cela une regexp pour differencier les lettres des chiffres. Il aurait ete possible, mais moins interessant, de les stocker sous forme d'une liste [col, row]
	var col0 = colToIdx(select[0].match(/[A-Z]+/)[0]);
	var row0 = select[0].match(/\d+/);
	var col1 = colToIdx(select[1].match(/[A-Z]+/)[0]);
	var row1 = select[1].match(/\d+/);

	// on determine les colonnes et les lignes de debut et de fin
	var colS = Math.min(col0, col1);
	var colE = Math.max(col0, col1);
	var rowS = Math.min(row0, row1);
	var rowE = Math.max(row0, row1);
	
	for (var i = colS; i <= colE; i++) {
	    for (var j = rowS; j <= rowE; j++) {
		var id = idxToCol(i) + j;
		var td = document.getElementById(id);
		f(td);
	    }
	}
    }


    // La fonction bold passe en gras la cellule passee en parametre si elle ne l'est pas deja, et sinon la remet en "normal". Idem pour underline et italic, avec respectivmement le style souligné et le style italique

    function bold (td) {
	if (td.style.fontWeight != "bold")
            td.style.fontWeight = "bold";
	else
	    td.style.fontWeight = "";
    }

    function italic (td) {
	if (td.style.fontStyle != "italic")
	    td.style.fontStyle = "italic";
	else
	    td.style.fontStyle = "";
    }

    function underline (td) {
	if (td.style.textDecoration != "underline")
	    td.style.textDecoration = "underline";
	else
	    td.style.textDecoration = "";
    }

    // Les fonctions color et bgColor prennent en parametre une case, une couleur, et changent la couleur de la case

    function color (td, c) {
	td.style.color = c;
    }
    
    function bgColor (td, c) {
	td.style.backgroundColor = c;
    }

    // Les fonctions bordStyle et bordColor prennent en parametre une case, une style ou une couleur, et changent le style de la case

    function bordStyle (td, s) {
	if (td.style.borderStyle != s)
	    td.style.borderStyle = s;
	else
	    td.style.borderStyle = "";
    }

    function bordColor (td, c) {
	td.style.borderColor = c;
    }


    // On modifie le code permettant de sélectionner une case. On considère que lorsqu'on clique sur une case, la selection devient la liste correspondant a deux fois l'id de cette case (elle seule est selectionnee), et quand on clique sur une case avec la touche ctrl enfoncee on selectionne toutes les cases entre ces deux cases, et this.selection devient la liste de leurs ids.

    this_.selection = null;


    function tdClickHandler (e ) {

        var td = findTD(e.target);
        if (!td) {
            this_.selection = null;
            return;
        };

	// s'il y a deja une selection, on repasse tous les elements de la selection en "non-selectionne"
        if (this_.selection)
            forSelect(function (td) {td.select(false);}, this_.selection);
	// on initialise la selection avec la seule cellule selectionnee pour l'instant
        this_.selection = [td.id, td.id];
	td.select(true);

        var cell = view.model.getCell(td.col, td.row);
        var form = cell.getFormula();
        view.input.value = form ? '=' + form.toString() : "";

        //focus the input.

        setTimeout( function () { view.input.focus(); }, 100);

    };

    function tdCtrlClickHandler (e) {

	var td = findTD(e.target);

	// si ce n'est pas sur une case que l'on a clique, ou si rien n'a deja ete selectionne
	if ((!td) || (!this_.selection)) {
	    return;
	};

        if (this_.selection)
            forSelect(function (td) {td.select(false);}, this_.selection);
	
	// on selectionne toute la zone
	this_.selection = [this_.selection[0], td.id];
	forSelect(function (td) {td.select(true);}, this_.selection);

	//focus the input.

        setTimeout( function () { view.input.focus(); }, 100);
    }

    view.table.addEventListener("mousedown", function (e) {
	if (e.ctrlKey == true) //touche ctrl appuyee
	    tdCtrlClickHandler(e);
	else
	    tdClickHandler(e);
    });



    // On modifie la fonction de telle sorte que lorsque l'on appuye sur le bouton ou la touche entree, seule la premiere case de la selection est modifiee pour afficher la valeur de l'input
    function buttonClickHandler (e) {
	// On recupere l'element dont l'id est le premier de la selection
        var td = document.getElementById(this_.selection[0]);
        if (!td) return;
	var s = view.input.value;
	var cell = view.model.getCell(td.col, td.row);

	//test if it is a formula:
	var res = s.match(/^=(.*)$/);
	try {
	    var address = td.col + "," + td.row;
	    if (res)
		cell.setFormula(res[1], address);
	    else
		cell.setFormula('"' + s + '"', address);
	} catch (e) {
	    alert(e);
	}
    };

    view.button.addEventListener("click", buttonClickHandler);
    view.input.addEventListener("keypress", function (e) {
	if (e.keyCode == 13) //[enter]
	    buttonClickHandler(e);
    });

    
    // Ajout de code pour le projet

    // Fonction qui gerera le clic sur un des boutons de style : selon le bouton, elle lance la fonction correspondante sur la selection avec la fonction forSelect
    function styleClickHandler (ev) {

	// on recupere la case selectionnee
	var select = this_.selection;
	if (!select) return;

	var id = ev.target.id;

	switch (id)
	{
	    case "bold":
	    forSelect(bold, select);
	    break;

	    case "italic":
	    forSelect(italic, select);
	    break;

	    case "underline":
	    forSelect(underline, select);
	    break;

	    default: return;
	}

    }
    // on ajoute l'evenement qui reagit au clic sur la case du bouton gras
    view.boldButton.addEventListener("click", styleClickHandler);

    // on ajoute l'evenement qui reagit au clic sur la case du bouton italic
    view.italButton.addEventListener("click", styleClickHandler);

    // on ajoute l'evenement qui reagit au clic sur la case du bouton underline
    view.underButton.addEventListener("click", styleClickHandler);


    // Fonction qui gere le clic sur les boutons de couleurs de bg et de texte
    function colorClickHandler (ev) {

	var select = this_.selection;
	if (!select) return;

	var id = ev.target.id;

	// quand le bouton est clique, on recupere la valeur de la couleur selectionnee dans la liste
	var list = document.getElementById("listCol");
	var c = list.options[list.selectedIndex].value;

	// si c'est la case "Autre :", on recupere la couleur dans l'input, qui existe forcement puisqu'il est cree des que l'on clique sur "Autre :"
	if (c == "other") {
	    var input = document.getElementById("colorInput");
	    c = input.value;
	}

	// selon le bouton clique, on modifie la couleur du texte ou celle du bg. On utilise une fonction intermediaire parce qu'on ne peut pas passer la couleur en parametre si on passe directement color et bgColor a forSelect
	switch (id)
	{
	    case "color": forSelect(function (td) {color(td, c);}, select);
	    break;

	    case "bgCol": forSelect(function (td) {bgColor(td, c);}, select);
	    break;

	    default: return;
	    break;
	}

    }

    view.colorButton.addEventListener("click", colorClickHandler);
    view.bgColButton.addEventListener("click", colorClickHandler);


    // On gere le fait que quand on clique sur la case "Autre :", un input doit apparaitre
    function otherClickHandler (ev) {

	// on recupere le nombre d'element, l'index de la case selectionnee, et le div ou on va mettre l'input
	var n = ev.target.childElementCount;
	var ind = ev.target.selectedIndex;
	var div = document.getElementById("color_input-div");

	// on enleve les enfants existant potentiellement deja
	for (var c = div.firstChild; c != null; c = c.nextSibling)
            div.removeChild(c);

	// si l'indice selectionne est celui du dernier, c'est autre. On ajoute donc notre input
	if (ind == n - 1) {
	    var inp = document.createElement("input");
	    inp.id = "colorInput";
	    div.appendChild(inp);

	    setTimeout( function () { inp.focus(); }, 100);
	}

    }

    view.listColor.addEventListener("click", otherClickHandler);

    // On affiche un pop-up avec la fonction alert qui ouvre une fenetre controlee par le navigateur
    function helpClickHandler (ev) {

	alert("Pour utiliser une couleur autre que celles proposées au départ, choisir 'Autre :' dans la liste des options.\nApparaît alors une case dans laquelle vous pourrez écrire le nom de votre couleur, ou sa description hexadécimal.\n\nSi vous ne connaissez pas de couleurs, la palette vous en propose un certain nombre. Il y a juste à cliquer sur une couleur pour que son nom apparaisse.");

    }
    view.helpButton.addEventListener("click", helpClickHandler);


    
    function bordStyleClickHandler (ev) {

	var select = this_.selection;
	if (!select) return;

	var list = ev.target;
	var style = list.options[list.selectedIndex].value;
	
	switch (style)
	{
	    case "":
	    break;

	    case "solid":
	    forSelect(function (td) { bordStyle(td, style); }, select);
	    break;

	    case "dotted":
	    forSelect(function (td) { bordStyle(td, style); }, select);
	    break;
	    
	    case "dashed":
	    forSelect(function (td) { bordStyle(td, style); }, select);
	    break;
	    
	    default: return;
	    break;
	}
	
    }
    view.bordStyle.addEventListener("click", bordStyleClickHandler);

    function bordColorClickHandler (ev) {

	var select = this_.selection;
	if (!select) return;

	var list = ev.target;
	var col = list.options[list.selectedIndex].value;

	forSelect(function (td) { td.style.borderColor = col; }, select);
	
    }
    view.bordColor.addEventListener("click", bordColorClickHandler);

    // On ajoute un eventListener permettant de modifier le style avec des combinaisons de touche (ctrl+b pour bold, par exemple)

    document.addEventListener("keypress", function (e) {
	var select = this_.selection;
	if (!select) return;
	
	switch (e.keyCode)
	{
	    case 8221:
	    forSelect(bold, select);
	    break;
	    
	    case 8594:
	    forSelect(italic, select);
	    break;

	    case 8595:
	    forSelect(underline, select);
	    break;
		
	    default:
	    break;
	}
    });


    function receiveMessage (ev) {
	var inp = document.getElementById("colorInput");
	if (!inp)
	    return;
	inp.value = ev.data;
    }
    window.addEventListener("message", receiveMessage);
}

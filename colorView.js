var ColorView = function(id) {

    this.div = document.createElement("div");
    this.div.id = "colorTab-div";

    var target = document.getElementById(id);
    if (target)
	target.appendChild(this.div);

    this.color = [["silver", "darkgray", "slategray"],
	         ["burlywood", "peru", "sienna"],
		 ["greenyellow", "forestgreen", "darkgreen"],
		 ["powderblue", "cornflowerblue", "midnightblue"],
		 ["violet", "mediumorchid", "darkmagenta"],
		 ["darksalmon", "crimson", "darkred"],
		 ["gold", "orange", "darkorange"]];

    this.nbColor = this.color.length;
    this.nbVar = this.color[1].length;

    this.table = document.createElement("table");
    this.div.appendChild(this.table);

    this.output = document.createElement("div");
    this.output.id = "output";
    target.appendChild(this.output);
};

ColorView.prototype.createColTab = function () {

    var table = this.table;
    var colLis = this.color;
    var nbRow = this.nbColor;
    var nbCol = this.nbVar;

    // Clear the table
    for (var c = table.firstChild; c != null; c = c.nextSibling)
        table.removeChild(c);

    var thead = document.createElement("thead");
    table.appendChild(thead);


    var tbody = document.createElement("tbody");
    table.appendChild(tbody);

    for(var i = 0; i < nbRow; i++) {
	var tr = document.createElement("tr");
	tbody.appendChild(tr);

	for(var j = 0; j < nbCol; j++) {
	    var td = document.createElement("td");
	    var color = colLis[i][j];
	    td.style.backgroundColor = color;
	    td.id = color;
	    tr.appendChild(td);
	    td.col = j;
	}
    }
};
    

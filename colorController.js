var ColorController = function (view) {

    var this_ = this;

    this_.selection = null;
    
    if (!(view instanceof ColorView))
	throw "Invalid view";

    function findTD(obj) {
	if ( !obj || obj.nodeName == "TD") return obj
	else findTD(obj.parentNode);
    };

    function tdClickHandler (e) {

	var td = findTD(e.target);
        if (!td) {
            this_.selection = null;
            return;
        };
	
	var col = td.id;

	var output = document.getElementById("output");
	output.style.color = col;
	output.innerHTML = col;

	if (td.col == 0)
	    output.style.backgroundColor = "black";
	else
	    output.style.backgroundColor = "white";

	window.opener.postMessage(col, "*");
    }

    view.table.addEventListener("mousedown", tdClickHandler);
}
